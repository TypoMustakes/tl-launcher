# This launcher is outdated!
For the latest packaged version, visit [tl-legacy](https://gitlab.com/TypoMustakes/tl-legacy)!

This repository is only an archive of the last version published to the Arch User Repository before being deleted from there.
You can still install it if you wish, it works perfectly, but please note that Minecraft 1.17 and later versions are only supported as of TL Legacy 1.122.7, which is a newer version than what you are seeing here.

## How to install

### Prerequisites

> - pacman
>
> - either `git` or any archiving program`
>
> - root privileges

### Download

> ### with git:
>
> > `git clone https://gitlab.com/TypoMustakes/tl-launcher.git`
>
> ### without git:
>
> > - visit `https://gitlab.com/TypoMustakes/tl-launcher`
> >
> > - click the  (Download) button and choose and archive format
> >
> > - extract the archive

### Installation

> Open the project directory by typing `cd tl-launcher`
>
> ### installing existing package
> 
> > ```sudo pacman -U tlauncher-1:1.120.2-2-any.pkg.tar.zst```
> 
> ### Build your own package:
> 
> > ```makepkg -si```
